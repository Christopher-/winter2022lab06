public class ShutTheBox {
    public static void main(String [] args) {
        System.out.println("Welcome");
        board b = new board();
        boolean gameOver = false;
        //While game is not over run until value by a player has been reached
        do {
            System.out.println("Player 1's turn");
            System.out.println(b);
            if (b.playATurn() == true) {
                System.out.println("Player 2 wins");
                gameOver = true;
            } else {
                System.out.println("Player 2's turn");
                if (b.playATurn() == true) {
                    System.out.println("Player 1 wins");
                    gameOver = true;
                }
            }
        }
        while(gameOver != true);
    }
}