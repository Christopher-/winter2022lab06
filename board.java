public class board {
    private die x;
    private die y;
    private boolean[] closedTiles;
    //Constructor for board object
    public board() {
        x = new die();
        y = new die();
        closedTiles = new boolean[12];
    }
    // Returns the string array of the board with the numbers or values crossed out 
    // Changed index starting from 1-12 instead of 0-11 (visually)
    public String toString() {
        String closed = "";
        for (int count = 0; count < closedTiles.length;count++){
            if(closedTiles[count] == true) {
                closed += "x ";
            }
            else {
                closed += String.valueOf(count + 1 + " ");
            }
        }
		//Returns the values in the array 
        return closed; 
    }
    //Method for cycling through turns between each player
    public boolean playATurn() {
        // Randomizes each roll
        x.roll();
        y.roll();
        System.out.println("x die: " + x);
        System.out.println("y die: " + y);
        int sum = x.getPips() + y.getPips();
        // Check to see if value at the position has already occured
        if (closedTiles[sum-1] == false) {
            System.out.println("Closing Tile: " + (sum));
            closedTiles[sum-1] = true;
            return false;
        } else {
            System.out.println("Tile at position " + (sum) + " is already shut");
            return true;
        }
    }
}