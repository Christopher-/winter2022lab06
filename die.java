import java.util.Random;

public class die {
    private int pips;
    private Random r;
    //Constructor for die object
    public die() {
        this.pips = 1;
        this.r = new Random();
    }
    //Get method for pips
    public int getPips() {
        return this.pips;
    }
    //Get method for random object
    public Random getR() {
        return this.r;
    }
    //Rolls number between 1 - 6
    public void roll() {
        this.pips = this.r.nextInt(6)+1;
    }
    //Override toString method 
    public String toString() {
        return "Value for pips is: " + this.pips;
    }
}
